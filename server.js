const express = require('express')
const cors = require('cors')
const { scheduleJob } = require('node-schedule')
const { Initialize, PopulateDB, Calculate } = require('./src/redis-driver')
const AirtableFetcher = require('./src/airtable-fetcher')
const DistanceFetcher = require('./src/google-distance-fetcher')

require('dotenv').config()

var whitelist = process.env.CORS_WHITE_LIST ? process.env.CORS_WHITE_LIST.split(',') : []
var corsOptions = {
  origin: whitelist.length ? (origin, callback) => {
    if (whitelist.indexOf(origin) !== -1) {
      callback(null, true)
    } else {
      callback(new Error('Not allowed by CORS'))
    }
  } : false
}

const fetcher = AirtableFetcher({
  url: process.env.AIRTABLE_ENDPOINT,
  key: process.env.AIRTABLE_KEY,
  base: process.env.AIRTABLE_BASE
})
const distanceFetcher = DistanceFetcher({
  url: process.env.GOOGLE_DISTANCES_ENDPOINT,
  key: process.env.GOOGLE_API_KEY
})

// Set Port
const port = process.env.PORT || 3000

// Init app
const app = express()

// Get All Distances
app.get('/get/:long/:lat/:radius/:unit', cors(corsOptions), (req, res) => {
  const { lat, long, radius, unit } = req.params

  Calculate({ lat, long, radius, unit })
    .then(redisObj => {
      return distanceFetcher(
        {
          Latitude: redisObj.Latitude,
          Longitude: redisObj.Longitude
        },
        redisObj.hashes
      )
    })
    .then(hashes => res.json({
      success: true,
      results: hashes
    }))
    .catch(error => res.json({
      success: false,
      error: error.message
    }))
})

app.listen(port, () => {
  // Populates the DB on connect
  fetcher('Places', { filterByFormula: 'Approved' })
    .then(results => Initialize(results))
    .catch(() => {})
    .then(() => {
      console.log(`Server started on port ${port}`)
    })

  // Schedules a process once per day to re-fetch from airtable
  scheduleJob('0 0 * * *', () => {
    return fetcher('Places', { filterByFormula: 'Approved' })
      .then(results => PopulateDB(results))
  })
})
