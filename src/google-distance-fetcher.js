 const request = require('request-promise')

/*
 * @module DistanceFetcher
 * @description Takes the API_URL and an API_KEY and returns a function that uses it to calculate
 * the distance between an origin and an array with destinations
 * @param url {string} - With the distances api url
 * @param key {string} - With the API_KEY
 * @returns {function} - To call with just the origin and destinations to calculate distances
 */
module.exports = ({ url, key }) => {
  /*
   * Function to calculate distances
   * @param origin {object} - An object with a required signature { Latitude, Longitude }
   * @param destination {array} - Array with objects with the same signature as above
   * @returns {promise} - With the request to the API
   */
  return (origin, destinations) => {
    const params = {
      key: key,
      origins: `${origin.Latitude},${origin.Longitude}`,
      destinations: destinations
        .map(destination => `${destination.Latitude},${destination.Longitude}`)
        .join('|'),
      units: 'imperial'
    }
    const queryParams = Object.keys(params)
      .map(param => `${encodeURIComponent(param)}=${encodeURIComponent(params[param])}`)
      .join('&')
    const requestUrl = `${url}?${queryParams}`

    // The promise already returns the destinations array with all the info it had with the API
    // distance and duration objects
    return request({ uri: requestUrl, json: true })
      .then(res => {
        return destinations
          .map((destination, index) => {
            return {
              ...destination,
              googleDistance: res.rows[0].elements[index].distance || '',
              googleDuration: res.rows[0].elements[index].duration || ''
            }
          })
          .filter(destination => destination.googleDistance)
      })
  }
}
