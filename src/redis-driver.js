const redis = require('redis')
const { promisify } = require('util')

// Create Redis Client
const client = redis.createClient()
let connected = false
let onConnected = () => Promise.resolve()
client.on('connect', () => {
  connected = true
  onConnected()
    .then(() => {
      console.log('Connected to Redis...')
    })
})

// Make redis async function promises
const cleanRedis = promisify(client.flushall).bind(client)
const getAsync = promisify(client.hgetall).bind(client)
const calculateRadius = promisify(client.georadius).bind(client)

/*
 * PopulateDB fetches from airtable and populates redis
 * @param dataset {array} - With all the information to populate redis geohash and hashes
 * @returns {promise} - With resolve for the dataset
 */
const PopulateDB = (dataset) => {
  return Promise.resolve(dataset)
    .then(results => {
      return cleanRedis().then(() => results)
    })
    .then(results => {
      const gsIndexes = results.reduce((acc, { fields }) => {
        acc.push(fields.Longitude, fields.Latitude, fields['Google Place ID'])
        return acc
      }, [])
      client.geoadd('places', gsIndexes)

      return results
    })
    .then(results => {
      const hashes = results.reduce((acc, { fields }) => {
        acc[fields['Google Place ID']] = fields

        return acc
      }, {})

      Object.keys(hashes).forEach(gpID => {
        const currentHash = hashes[gpID]

        client.hset(gpID, Object.keys(currentHash).reduce((acc, key) => {
          acc.push(key, currentHash[key])
          return acc
        }, []))
      })

      return results
    })
}

/*
 * Calculate compares a point and a radius with all the geohash to return places within range
 * @param object {object} - With the information to calculate the radius { lat, long, radius, unit }
 * @returns {promise} - With a promise for the dataset
 */
const Calculate = ({ lat, long, radius, unit }) => {
  return calculateRadius('places', long, lat, radius, unit, 'WITHDIST', 'ASC')
    .then(distances => Promise.all(
      distances.map(distance => {
        return getAsync(distance[0])
          .then(hash => {
            hash.distance = distance[1]

            return hash
          })
      })
    ))
    .then(hashes => {
      return {
        Latitude: lat,
        Longitude: long,
        hashes
      }
    })
}

// The module
module.exports = {
  Initialize: (dataset) => {
    if (connected) {
      return PopulateDB(dataset)
    }

    onConnected = () => {
      return PopulateDB(dataset)
    }

    return Promise.resolve()
  },
  PopulateDB: PopulateDB,
  Calculate: Calculate
}
