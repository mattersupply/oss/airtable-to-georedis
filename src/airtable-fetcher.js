const Airtable = require('airtable')

/*
 * @module AirtableFetcher
 * @description Takes the config object to return a function that fetches information from airtable
 * @param config {object} - With { url, key, base } to configure the fetcher
 * @returns {function} - To call with just the datasource and filters to fetch data
 */
module.exports = (config) => {
  Airtable.configure({
    endpointUrl: config.url,
    apiKey: config.key
  })
  const base = Airtable.base(config.base)

  /*
   * Function to fetch airtable data
   * @param datasource {string} - With the tab name
   * @param filters {object} - With anything that extends the airtable api with extra parameters
   * @returns {promise} - With the request to airtable
   */
  return (datasource, filters) => {
    return new Promise((resolve, reject) => {
      let iterator = []

      const page = (records, fetchNextPage) => {
        iterator = [].concat(iterator, records)
        fetchNextPage()
      }
      const done = err => {
        if (err) {
          reject(err)
        } else {
          resolve(iterator)
        }
      }

      base(datasource)
        .select({
          view: 'Grid view',
          ...(filters && typeof filters === 'object' ? filters : {})
        })
        .eachPage(page, done)
    })
  }
}
