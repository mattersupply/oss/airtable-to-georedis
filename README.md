# Airtable to GeoRedis

Backend project built with Node.js, Airtable, Redis and Google Distance Matrix API to have airtable as datasource and process data through Redis to pre-filter locations and finally complement it with Google distances to complement walking distance information

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

The backend server runs on [Node.js v8.9 (LTS)](https://nodejs.org/en/blog/release/v8.9.0/) and Redis.

To have Node.js you can use [creationix NVM](https://github.com/creationix/nvm) script to help you out:
```
curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.33.11/install.sh | bash
nvm install 8.9
```

To install Redis you can download the latest tar.gz on the [homepage](https://redis.io/) or:

If you have apt-get on your linux
```
sudo apt-get install redis-server
```
Or you can use Homebrew on Mac
```
brew install redis
```

To test the redis installation run
```
redis-cli PING
```
If it answers `PONG` you have Redis working on your local machine

### Installing

To install the server, once you have the prerequisites run
```
npm install
```

## Running the server

Before running the project, you need to configure the enviroment variables

### ENV file

Copy the `example.env` file as `.env` in your project root folder and inside replace the following values with yours
```
PORT=THE_PORT_OF_YOUR_APP
AIRTABLE_ENDPOINT=https://api.airtable.com
AIRTABLE_KEY=YOUR_AIRTABLE_KEY
AIRTABLE_BASE=YOUR_AIRTABLE_BASE
GOOGLE_DISTANCES_ENDPOINT=https://maps.googleapis.com/maps/api/distancematrix/json
GOOGLE_API_KEY=YOUR_GOOGLE_API_KEY
CORS_WHITE_LIST=COMA_SEPARATED_ALLOWED_ORIGIN_URLS
```

### Deploy

To deploy the server you just need to run
```
npm start
```

### Endpoints

The server exposes the following routes:

`/get/:long/:lat/:radius/:unit`: This endpoint responds a JSON with the nearby locations based on the input params `long`, `lat`, `radius`, `unit`

* `long:` stands for Longitude and goes from `-180` to `180`
* `lat:` stands for Latitude and its range can be calculated using `atan(sinh(PI)) * 180 / PI` for the limits `-85.05112878...` to `85.05112878...`
* `radius:` the distance to filter the nearby locations
* `unit:` the units for the distance above. Possible values are `mi` for Miles, `ft` for feet, it also supports `km` and `m` for the metric system but the standard for this app is the `imperial system`

e.g.
```
/get/151/-33/300/ft
/get/81.0334212/33.12312/20/mi
```

### Server processes

The server runs the Redis database population from airtable when the server launches for the first time, and once everyday at midnight (Server time). The required data from airtable is:

* `Latitude:` This column needs to exist on airtable with the latitude information for each row (place)
* `Longitude:` This column needs to exist on airtable with the longitude information for each row (place)
* `Google Place ID:` This column needs to exist on airtable with the "Google Place ID" information for each row (place)
* `Approved:` This column needs to exist on airtable with a flag (`true`/`false`) since on Redis will be just approved places

Any other information will be stored on Redis with the same attribute name as its column on airtable to be used that way on any place that uses this endpoint

## Other project notes

### Code style

1. Avoid callback hell and use promises

```
functionThatReturnsAPromise(...params)
  .then(result => {
    // do something
    return toPassToNextThen
  })
  .then(...etc)
```

2. Use arrow function notation

`No input parameters`
```
() => {
}
```

`One input parameters`
```
param => {
}
```

`More than one input parameters`
```
(param1, param2, etc...) => {
}
```

`Array functions => Arrow function transformation notation`
```
['something', 'something'].map(item => `Transformation with the ${item}`)
```

3. Node.js `require` instead of babel `imports`

```
const express = require('express')
const { Something, Method } = require('your-module')
```

4. Node.js `module.exports` instead of babel `export default ...`

```
module.exports = () => { ... }
```

## Built With

* [Express](https://expressjs.com/) - Fast, unopinionated, minimalist web framework for Node.js
* [CORS](https://github.com/expressjs/cors) - a Connect/Express middleware that can be used to enable Cross Origin Resource Sharing.
* [Airtable](https://github.com/airtable/airtable.js) - The official Airtable JavaScript library
* [Redis](https://github.com/NodeRedis/node_redis) - A Node.js Redis client
* [Bluebird promises](https://github.com/request/request-promise) - The simplified HTTP request client 'request' with Promise support. Powered by Bluebird.
* [Node Schedule](https://github.com/node-schedule/node-schedule) - A flexible cron-like and not-cron-like job scheduler for Node.js

## Contributors

* **David Gomez Fonnegra** - *Initial work* - [lowlander1982](https://github.com/lowlander1982)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details
